import pygame


black=(200,200,200)

start = [500,200]
const = 15 #vzdalenost ctverecku

class mrizka:
    def __init__(self):
        pass
    def draw(self,screen,brclt):
        for i in range(0,brclt.n_rows):
            for n in brclt.brac[i].nodes:
                color = n.l_color
                if n.color:
                    color = n.r_color
                
                px=start[0]+n.x*const*2+n.odd*const
                py=start[1]+n.y*const
                
                pygame.draw.polygon(screen,color,[(px-const,py),(px,py+const),(px+const,py),(px,py-const)],0)
                pygame.draw.polygon(screen,black,[(px-const,py),(px,py+const),(px+const,py),(px,py-const)],3)
        pygame.display.flip()
