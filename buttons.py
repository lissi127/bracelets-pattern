import pygame

pygame.font.init()
myfont = pygame.font.SysFont(None,25)
black = (0,0,0)

class buttons:
    def __init__(self):
        self.ar = []

class button:
    def __init__(self, x,y,name,d_name, color=(255,255,0)):
        self.color = color #barva tlacitka
        self.x = x #x pozice leveho horniho rohu
        self.y = y #y pozice leveho horniho rohu
        self.xl = 100 #x delka tlacitka
        self.yl = 50 #y delka tlacitka
        self.name = name #jmeno v kodu
        self.d_name = d_name #jmeno, ktere se vykresli na obrazovku
    def draw(self,screen):
        pygame.draw.rect(screen,self.color,(self.x,self.y,self.xl,self.yl),0)
        pygame.draw.rect(screen,black,(self.x,self.y,self.xl,self.yl),3)

        textsurface = myfont.render(self.d_name,False,(200,100,200))
        textRect = textsurface.get_rect(center=(self.x+int(self.xl/2),self.y+int(self.yl/2)))
        screen.blit(textsurface,textRect)
        pygame.display.flip()
