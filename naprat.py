from bracelet import *
from colors import *
from mrizka import *
from buttons import *
from generate2 import *

bgcolor =(255,255,255)
black = (0,0,0)
red = (255,0,0)
width = 1000
height = 800

keys = [256, 257,258,259, 260, 261, 262, 263, 264, 265, 44]
keys_dict = {256:0, 257:1,258:2,259:3, 260:4, 261:5, 262:6, 263:7, 264:8, 265:9, 44:","}
class App:
    def __init__(self, n_strings=8, n_rows=10):
        assert n_strings%2==0, "Provazku je licho"
        self.size = self.width, self.height = width,height
        self.screen = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF) 
        self.screen.fill(bgcolor)

        self._running = True
        self.n_strings = n_strings
        self.n_rows = n_rows
        self.bracelet = bracelet(n_strings, n_rows)
        self.bracelet.count_sons()
        self.color_row=color_row(n_strings, self.bracelet)
        self.input_box=input_box()
        self.mrizka = mrizka()
        
        self.buttons = buttons()
        addr = button(400,50,"add_row","Add row")
        remr = button(525,50,"rem_row","Rem row")
        adds = button(650,50,"add_str","Add str", (255,100,0))
        rems = button(775,50,"rem_str","Rem str", (255,100,0))
        gen = button(775,700,"gen","Generate", (255,100,255))
        self.buttons.ar.append(addr)
        self.buttons.ar.append(remr)
        self.buttons.ar.append(adds)
        self.buttons.ar.append(rems)
        self.buttons.ar.append(gen)

        self.str_sq=str_seq(self.n_strings)

        self.eve=1
        self.render = render()
        self.loop=loop()
        
        self.bracelet.brac[0].nodes[0].change_incolor_r(red)

    def on_event(self, event):
        if event.type==pygame.QUIT:
            self._running=False
        elif event.type==pygame.MOUSEBUTTONDOWN:
            self.eve=1
            mouse_pos = pygame.mouse.get_pos()
            self.loop.on_click(self.screen, mouse_pos, self.bracelet, self.input_box, self.buttons.ar,self.color_row, self.str_sq)
        elif event.type==pygame.KEYDOWN:
            self.eve=1
            
            if self.input_box.is_open:
                for k in keys:
                    if event.key == k:
                        self.input_box.input_text(str(keys_dict[k])) 
            
            if event.key == pygame.K_RETURN:
                if self.input_box.is_open:
                    color = self.input_box.close()
                    if color != None and color!=False:
                        if self.bracelet.changing%2 ==0:
                            self.bracelet.brac[0].nodes[int(self.bracelet.changing/2)].change_incolor_l(color)
                        else:
                            self.bracelet.brac[0].nodes[int(self.bracelet.changing/2)].change_incolor_r(color)
            
    def on_loop(self):
        pass
    def on_render(self):
        if self.eve:
            self.screen.fill(bgcolor)
            self.render.draw_bracelet(self.bracelet, self.screen)
            self.render.draw_mrizka(self.mrizka,self.bracelet, self.screen)
            self.render.draw_colors(self.color_row,self.screen)
            self.render.draw_buttons(self.buttons,self.screen)
            if self.input_box.is_open:
                self.render.draw_text_box(self.input_box, self.screen)
        self.eve = 0
    def on_execute(self):
        while self._running:
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()


creative = App()
creative.on_execute()
