import pygame,random

normal_colors = [(255,0,0),(0,255,0),(0,0,255),(255,255,0),(0,255,255),(255,0,255),(255,127,0),(255,0,127),(0,255,127),(0,127,255),(127,0,255),(127,255,0)]

class str_seq:
    def __init__(self,x):
        self.ar = [] #generuje cross (symetricky)
        self.clr = [] #generuje kera barva je nahore (symetricky)
        self.x=x #pocet provazku
        self.n=int(x/2) #pocet uzlu
        self.strclr = [] #generuje kerou barvu ma kery provazek (symetricky)
        self.colors =[] #pole moznych barev
        self.normal = 0
    def gen_row(self,odd):
        self.ar.append("")
        self.clr.append("")
        if odd:
            licho = self.n%2
            for i in range(0,int(self.n/2)+licho):
                cross = random.randint(0,1)
                if cross:
                    self.ar[-1]+="1"
                else:
                    self.ar[-1]+="0"
                color = random.randint(0,1)
                
                if color:
                    self.clr[-1]+="1"
                else:
                    self.clr[-1]+="0"

            si = ""
            ci=""
            for i in range(1+licho,len(self.ar[-1])+1):
                si+=self.ar[-1][-i]
                
                neco=self.clr[-1][-i]
                if neco=="1":
                    neco="0"
                else:
                    neco="1"
                ci+=neco
            self.ar[-1]+=si
            self.clr[-1]+=ci
        else:
            licho = (self.n-1)%2
            magic = int((self.x-2)/4)+(self.n-1)%2
            for i in range(0,magic):
                cross = random.randint(0,1)
                if cross:
                    self.ar[-1]+="1"
                else:
                    self.ar[-1]+="0"
                
                color = random.randint(0,1)
                if color:
                    self.clr[-1]+="1"
                else:
                    self.clr[-1]+="0"
            si = ""
            ci=""
            for i in range(1+licho,len(self.ar[-1])+1):
                si+=self.ar[-1][-i]
                    
                neco=self.clr[-1][-i]
                if neco=="1":
                    neco="0"
                else:
                    neco="1"
                ci+=neco

            self.ar[-1]+=si
            self.clr[-1]+=ci


    def generate(self, number):
        for i in range(0,number):
            odd = (i%2+1)%2
            self.gen_row(odd)
        for i in range(1,2*len(self.ar)-2):
            if i%2==1:
                continue
            self.ar.append(self.ar[-i])
            self.clr.append(self.clr[-i])

    def gen_color(self):
        a = random.randint(0,255)
        b = random.randint(0,255)
        c = random.randint(0,255)
        return (a,b,c)

    def generate_clrs(self,n):#n je pocet barev, ktere clovek v naramku chce
        self.strclr = []
        self.colors = []
        
        if self.normal==0:
            for i in range(0,n):
                barva = self.gen_color()
                self.colors.append(barva)
        else:
            for i in range(0,n):
                x = random.randint(0,len(normal_colors)-1)
                barva = normal_colors[x]
                self.colors.append(barva)
        
        for i in range(0,int(self.x/2)):
            a = random.randint(0,n-1)
            self.strclr.append(a)
        for i in range(1,2*len(self.strclr)+1):
            if i%2==0:
                continue
            self.strclr.append(self.strclr[-i])

    def do_gen(self,brclt,number,n_colors):
        self.normal = 1
        self.ar=[]
        self.clr=[]
        self.x=brclt.n_strings
        self.n = int(brclt.n_strings/2) 
        self.generate(number)
        for i in range(0,len(brclt.brac)): #zmeni cross a barvu nahore podle vygenerovanych hodnot
            seq = self.ar[i%len(self.ar)]
            seqclr = self.clr[i%len(self.clr)]
            for n in brclt.brac[i].nodes:
                if seq[n.x]=="0" and n.cross==1:
                    n.change_cross()
                elif seq[n.x]=="1" and n.cross==0:
                    n.change_cross()
                if seqclr[n.x]=="0" and n.color==1:
                    n.change_color()
                elif seqclr[n.x]=="1" and n.color==0:
                    n.change_color()
        
        self.generate_clrs(n_colors)
        for i in range(0,self.x):
            barva=self.colors[self.strclr[i]]
            if i%2==0:
                brclt.brac[0].nodes[int(i/2)].change_incolor_l(barva)
            else:
                brclt.brac[0].nodes[int(i/2)].change_incolor_r(barva)
