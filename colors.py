import pygame 

col_rad=20
black = (0,0,0)
box_x=200
box_y=50
pygame.font.init()
myfont = pygame.font.SysFont(None,40)

class colors:
    def __init__(self,x,color, pos, dist):
        self.x=x
        self.pos_x=pos[0]+x*dist
        self.pos_y=pos[1]
        self.color=color
    def draw(self,screen):
        pygame.draw.rect(screen,self.color, (self.pos_x,self.pos_y,col_rad,col_rad),0)

class color_row:
    def __init__(self,x, brclt):
        self.colors=[]
        self.brclt = brclt
        self.x=x
        self.pos = [50,125]
        self.dist = 30

        for i in range(0,x):
            n = brclt.brac[0].nodes[int(i/2)]
            color = n.l_color
            if i%2==1:
                color=n.r_color
            new_color = colors(i,color,self.pos,self.dist)
            self.colors.append(new_color)
    
    def add_color(self):
        self.x+=1
        new_color = colors(self.x-1,black, self.pos, self.dist)
        self.colors.append(new_color)
    def rem_color(self):
        self.x-=1
        self.colors.pop()

    def draw(self,screen):
        for i in range(0,self.x):
            n = self.brclt.brac[0].nodes[int(i/2)]
            color = n.l_color
            if i%2==1:
                color=n.r_color
            self.colors[i].color = color
        for c in self.colors:
            c.draw(screen)
        pygame.display.flip()

class input_box:
    def __init__(self):
        self.pos_x=0
        self.pos_y=0
        self.text = ""
        self.is_open=0
    def open(self, screen):
        self.is_open=1
        pygame.draw.rect(screen,(0,0,255), (self.pos_x,self.pos_y,box_x, box_y),0)
        pygame.draw.rect(screen,(0,0,0), (self.pos_x,self.pos_y,box_x, box_y),2)
        textsurface = myfont.render(self.text,False,(200,100,200))
        textRect = textsurface.get_rect(center=(100,25))
        screen.blit(textsurface,textRect)
        
        
        pygame.display.flip()
    def convert(self,text):
        color=[0,0,0]
        s=""
        pocet = 0
        for i in text:
            if i != ",":
                s+=i
                continue
            try:
                s=int(s)
                if s<=255 and s>=0:
                    color[pocet]=s
                    s=""
                    pocet+=1
                    if pocet>2:
                        return False
            except ValueError:
                return False
        try:
            s=int(s)
            if s<=255 and s>=0:
                color[pocet]=s
                return color
        except ValueError:
            return False
    
    def input_text(self,pismeno):
        if len(self.text)<11:
            self.text+=pismeno
    def close(self):
        text = self.convert(self.text)
        self.is_open=0
        self.text=""
        return text
