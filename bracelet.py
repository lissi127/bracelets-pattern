import pygame,math

start = [50,200]
dist = 30 #vzdalenost mezi uzly
radius = 10 #polomer uzlu
arr_rad=3   
click_tol = 10  #tolerance na kliknuti
black=(0,0,0)
diag_x = radius/math.sqrt(5) #odkud se ma kreslit spojnice
diag_y = 2*diag_x

start_col=[50,100]
col_dist=40
col_tol=15

class node:
    def __init__(self, x, y, odd, n_nodes, right=0, color = 1, cross = 0):
        self.x=x #pozice v naramku
        self.y=y
        self.pos_x = start[0]+x*dist+odd*(int(dist/2)) #realna pozice na obrazovce
        self.pos_y = start[1]+y*dist
        self.color=color #jestli je videt barva leveho nebo praveho provazku
        self.cross=cross #jestli se provazky krizi nebo ne
        self.l_color = black #barva leveho provazku co vchazi
        self.r_color = black
        self.l_son = -1 #levy syn - index v naramku
        self.r_son = -1
        self.odd = odd #zda je v lichem radku
        self.n_nodes=n_nodes #pocet uzlu v danem radku
        self.arrow = arrow(right) 
        
    def change_incolor_l(self,color):
        self.l_color=color
        l=color
        r=self.r_color
        if self.cross:
            l,r=r,l
            if self.r_son!=-1:
                if not self.odd and not self.r_son.odd:
                    self.r_son.change_incolor_r(r)
                else:
                    self.r_son.change_incolor_l(r)
        elif self.l_son!=-1:
            if not self.odd and not self.l_son.odd:
                self.l_son.change_incolor_l(l)
            else:
                self.l_son.change_incolor_r(l)
     
    def change_incolor_r(self,color):
        self.r_color=color
        r=color
        l=self.l_color
        if self.cross:
            l,r=r,l
            if self.l_son!=-1:
                if not self.odd and not self.l_son.odd:
                    self.l_son.change_incolor_l(l)
                else:
                    self.l_son.change_incolor_r(l)
        elif self.r_son!=-1:
            if not self.odd and not self.r_son.odd:
                self.r_son.change_incolor_r(r)
            else:
                self.r_son.change_incolor_l(r) 
    
    def change_color(self):
        self.color=(self.color+1)%2
    def change_cross(self):
        self.cross=(self.cross+1)%2
        
        l=self.l_color
        r=self.r_color
        if self.cross:
            l,r=r,l

        if self.l_son !=-1:
            if not self.odd and not self.l_son.odd:
                self.l_son.change_incolor_l(l)
            else:
                self.l_son.change_incolor_r(l)
        if self.r_son !=-1:
            if not self.odd and not self.r_son.odd:
                self.r_son.change_incolor_r(r)
            else:
                self.r_son.change_incolor_l(r)

    def draw(self, screen, n_nodes, n_rows):
        color=self.l_color
        if self.color:
            color = self.r_color
        pygame.draw.circle(screen,color,[self.pos_x,self.pos_y],radius) #screen,color, [pos], radius, thickness
        pygame.draw.circle(screen,(0,0,0),[self.pos_x,self.pos_y],radius,1) #screen,color, [pos], radius, thickness
        self.arrow.draw(screen, self.pos_x,self.pos_y)
        
        l=self.l_color
        r=self.r_color
        if self.cross:
            l=self.r_color
            r=self.l_color
        
        if self.y==0: #pokud je prvni
            pygame.draw.line(screen, self.l_color, [self.pos_x-diag_x,self.pos_y-diag_y],[self.pos_x-(dist/2)+diag_x,self.pos_y-dist+diag_y],3) 
            pygame.draw.line(screen, self.r_color, [self.pos_x+diag_x,self.pos_y-diag_y],[self.pos_x+(dist/2)-diag_x,self.pos_y-dist+diag_y],3)
        
        if self.x ==0 and (not self.odd): #pokud je vlevo a v sude rade
            pygame.draw.line(screen, l, [self.pos_x-diag_x,self.pos_y+diag_y],[self.pos_x-(dist/2),self.pos_y+dist],3) 
            pygame.draw.line(screen, r, [self.pos_x+diag_x,self.pos_y+diag_y],[self.pos_x+(dist/2)-diag_x,self.pos_y+dist-diag_y],3)
            if self.y!=n_rows-1:
                pygame.draw.line(screen, l, [self.pos_x-(dist/2),self.pos_y+dist],[self.pos_x-diag_x,self.pos_y+2*dist-diag_y],3)
        elif self.x == n_nodes-1 and (not self.odd): #pokud je nejvic vpravo a v sude rade
            pygame.draw.line(screen, l, [self.pos_x-diag_x,self.pos_y+diag_y],[self.pos_x-(dist/2)+diag_x,self.pos_y+dist-diag_y],3) 
            pygame.draw.line(screen, r, [self.pos_x+diag_x,self.pos_y+diag_y],[self.pos_x+(dist/2),self.pos_y+dist],3)
            if self.y!=n_rows-1:
                pygame.draw.line(screen, r, [self.pos_x+(dist/2),self.pos_y+dist],[self.pos_x+diag_x,self.pos_y+2*dist-diag_y],3)
        else:
            pygame.draw.line(screen, l, [self.pos_x-diag_x,self.pos_y+diag_y],[self.pos_x-(dist/2)+diag_x,self.pos_y+dist-diag_y],3) 
            pygame.draw.line(screen, r, [self.pos_x+diag_x,self.pos_y+diag_y],[self.pos_x+(dist/2)-diag_x,self.pos_y+dist-diag_y],3)

class arrow:
    def __init__(self, right,color=(0,0,0)):
        self.right = right
    def draw(self, screen,x,y):
        if self.right:
            pygame.draw.line(screen, black, [x-arr_rad,y-arr_rad],[x+arr_rad,y+arr_rad],1) #screen, color, start_pos, end_pos, width 
        else:
            pygame.draw.line(screen, black, [x+arr_rad,y-arr_rad],[x-arr_rad,y+arr_rad],1) #screen, color, start_pos, end_pos, width 

class row:
    def __init__(self, x, y, odd = 1):
        self.x=x
        self.y=y
        self.odd=odd
        self.nodes = []
        self.n_nodes = (int(x/2)-1)
        if not odd: 
            self.n_nodes+=1
        for i in range(0,self.n_nodes):
            new_node=node(i,y,odd,self.n_nodes)
            self.nodes.append(new_node)
    def draw(self, screen, n_rows):
        for i in range(0,len(self.nodes)):
            n=self.nodes[i]
            n.draw(screen,self.n_nodes, n_rows)

class bracelet:
    def __init__(self, n_strings, n_rows):	
        self.brac = []
        self.n_strings = n_strings
        self.n_rows = n_rows
        self.changing = 0
        for i in range(0,n_rows):
            new_row = row(n_strings, i, i%2)
            self.brac.append(new_row)

    def count_sons(self):
        for i in range(0,self.n_rows-1):
            for n in self.brac[i].nodes:
                if n.x==0 and (not n.odd):
                    n.r_son=self.brac[n.y+1].nodes[n.x+n.odd]
                    if n.y==self.n_rows-2:
                        continue
                    n.l_son = self.brac[n.y+2].nodes[0]
                elif n.x==n.n_nodes-1 and (not n.odd): 
                    n.l_son=self.brac[n.y+1].nodes[n.x-1+n.odd]
                    if n.y==self.n_rows-2:
                        continue
                    n.r_son = self.brac[n.y+2].nodes[n.n_nodes-1]               
                else:
                    n.l_son=self.brac[n.y+1].nodes[n.x-1+n.odd]
                    n.r_son=self.brac[n.y+1].nodes[n.x+n.odd]
   
    def add_row(self):
        if self.n_rows<20:
            self.n_rows+=1
            new_row = row(self.n_strings,len(self.brac),len(self.brac)%2)
            self.brac.append(new_row)
            self.count_sons()
            for n in self.brac[0].nodes:
                n.change_incolor_l(n.l_color)
                n.change_incolor_r(n.r_color)

    def rem_row(self):
        if self.n_rows>1:
            self.n_rows-=1
            self.brac.pop()

    def add_str(self, clr_rw):
        if self.n_strings>26:
            return False
        self.n_strings+=2
        for r in self.brac:
            r.n_nodes+=1
            for n in r.nodes:
                n.n_nodes+=1
            new_node = node(r.n_nodes-1,r.y,r.odd,r.n_nodes)
            r.nodes.append(new_node)
        self.count_sons()

        for n in self.brac[0].nodes:
            n.change_incolor_l(n.l_color)
            n.change_incolor_r(n.r_color)
        clr_rw.add_color()
        clr_rw.add_color()
    
    def rem_str(self,clr_rw):
        if self.n_strings<6:
            return False
        self.n_strings-=2
        clr_rw.rem_color()
        clr_rw.rem_color()
        for r in self.brac:
            r.n_nodes-=1
            for n in r.nodes:
                n.n_nodes-=1
            r.nodes.pop()
        self.count_sons()
        for n in self.brac[0].nodes:
            n.change_incolor_l(n.l_color)
            n.change_incolor_r(n.r_color)

    def draw(self, screen):
        for r in self.brac:
            r.draw(screen, self.n_rows)
        pygame.display.flip()

class render:
    def draw_bracelet(self,brclt,screen):
        brclt.draw(screen)
    def draw_colors(self,color_row, screen):
        color_row.draw(screen)
    def draw_text_box(self,input_box,screen):
        input_box.open(screen)
    def draw_mrizka(self,mrizka,brclt,screen):
        mrizka.draw(screen, brclt)
    def draw_buttons(self,buttons, screen):
        for b in buttons.ar:
            b.draw(screen)
class loop:
    def on_click(self, screen, mouse_pos, brclt,input_box,butt_ar,clr_rw,str_sq):
        if input_box.is_open==1:
            return

        b=butt_ar[-1]
        if(mouse_pos[1]>start[1]-radius and mouse_pos[0]<start[0]+int(brclt.n_strings/2)*dist+radius): #pokud bylo kliknuto na naramek
            for r in brclt.brac:
                for n in r.nodes:
                    if abs(mouse_pos[0]-n.pos_x)<click_tol and abs(mouse_pos[1]-n.pos_y)<click_tol:
                        n.arrow.right=(n.arrow.right+1)%2
                        if n.color ==0 and n.cross==0:
                            n.change_color()
                        elif n.color==1 and n.cross==0:
                            n.change_color()
                            n.change_cross()
                        elif n.color==0 and n.cross==1:
                            n.change_color()
                        else:
                            n.change_color()
                            n.change_cross()
        elif mouse_pos[0]>butt_ar[0].x and mouse_pos[1]<butt_ar[0].y+butt_ar[0].yl: #pokud bylo kliknuto na button
            for b in butt_ar:
                if abs(b.x+int(b.xl/2)-mouse_pos[0])<int(b.xl/2) and abs(b.y+int(b.yl/2)-mouse_pos[1])<int(b.yl/2):
                    #print("heej co na mne klikas", b.name)
                    if b.name=="add_row":
                        brclt.add_row()
                    elif b.name=="rem_row":
                        brclt.rem_row()
                    elif b.name=="add_str":
                        brclt.add_str(clr_rw)
                    elif b.name=="rem_str":
                        brclt.rem_str(clr_rw)
        
        elif abs(b.x+int(b.xl/2)-mouse_pos[0])<int(b.xl/2) and abs(b.y+int(b.yl/2)-mouse_pos[1])<int(b.yl/2):
            str_sq.do_gen(brclt,4,3)
        else:#pokud bylo kliknuto na barvicky
            for i in range(0,brclt.n_strings):
                if abs(mouse_pos[0]-clr_rw.pos[0]-i*clr_rw.dist)<col_tol and abs(mouse_pos[1]-clr_rw.pos[1])<col_tol:
                    input_box.is_open=1
                    brclt.changing = i
                    print(i)
